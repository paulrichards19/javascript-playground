



	var MainView = Backbone.View.extend({

	  el: $('.holder'),

	  // Cache the template function for a single item.
	  todoTpl: _.template( "An example template" ),

	  events: {
		'dblclick label': 'edit',
		'keypress .edit': 'updateOnEnter',
		'blur .edit':   'close'
	  }

	});

	var mainView = new MainView();

	// log reference to a DOM element that corresponds to the view instance
	console.log(mainView.el); // logs <li></li>

