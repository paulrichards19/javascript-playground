
require.config({
    baseUrl: 'scripts',
    paths: {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        jquery: 'lib/jquery',
		Backbone : 'lib/backbone-min',
		underscore : 'lib/underscore-min'
    }
});

define(["jquery", "Backbone", "text!templates/home.html"], function( $, Backbone, HomeView) {
	
	var MainView = Backbone.View.extend({

	  el: $('.holder'),

	  // Cache the template function for a single item.
	  todoTpl: _.template( HomeView ),

	  events: {
		'dblclick label': 'edit',
		'keypress .edit': 'updateOnEnter',
		'blur .edit':   'close'
	  }

	});

	var mainView = new MainView();

	// log reference to a DOM element that corresponds to the view instance
	console.log(mainView.el); // logs <li></li>
	
});



